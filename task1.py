import json
from utils import create_tag


class WrongJsonTypeException(Exception):
    pass


def gen_html(data):
    json_data = json.loads(data)
    result = bytes()

    if type(json_data) is not list:
        raise WrongJsonTypeException

    for d in json_data:
        result += create_tag('h1', d['title'])
        result += create_tag('p', d['body'])

    return result
