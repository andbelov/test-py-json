import sys
from task5 import gen_html


# функции ввода-вывода предлагается протестировать вручную в среде, похожей на продакшен
def read_data(filename):
    """
    Reads text data from a given file
    :param filename: file name to read
    :return: string containing data read
    """
    with open(filename, 'r') as file:
        return file.read()


def write_data_to_stdout(data):
    """
    Writes encoded (UTF-8) text data to stdout
    :param data: string containing text data
    :return: None
    """
    sys.stdout.buffer.write(data)


def run_app(input_fn, output_fn, convert_fn):
    output_fn(convert_fn(input_fn()))


if __name__ == "__main__":
    run_app(
        input_fn=lambda: read_data('source.json'),
        output_fn=write_data_to_stdout,
        convert_fn=gen_html
    )
