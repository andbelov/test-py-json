from lxml import etree


def create_tag(tag_name, tag_text):
    el = etree.Element(tag_name)
    el.text = tag_text
    return etree.tostring(el, encoding='UTF-8', xml_declaration=False)
