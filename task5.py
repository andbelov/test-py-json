import json
from collections import OrderedDict
from lxml import etree


def gen_html(data):
    json_data = json.loads(data, object_pairs_hook=OrderedDict)

    root_tag = etree.Element('root')
    gen_obj(json_data, root_tag)

    # Ситуация осложнена тем, что для реализации алгоритма нам нужен единый корневой тэг, и этот корневой тэг не должен
    # присутствовать в результирующем HTML. Поэтому приходится итерировать субтэги у root_tag
    result = bytes()
    for sub_tag in root_tag:
        result += etree.tostring(sub_tag, encoding='UTF-8', xml_declaration=False)
    return result


def gen_obj(data_obj, parent_obj):
    """
    Attaches representation of a given json object to parent_obj
    :param data_obj: json object to convert
    :param parent_obj: etree.Element object to attach data to
    :return: None
    """
    if isinstance(data_obj, list):
        gen_list(data_obj, parent_obj)
    elif isinstance(data_obj, dict):
        gen_dict(data_obj, parent_obj)
    else:
        parent_obj.text = str(data_obj)


def gen_list(list_obj, parent_obj):
    """
    Attaches representation of a given json list object to parent_obj
    :param list_obj: list to convert
    :param parent_obj: etree.Element object to attach data to
    :return: None
    """
    ul_tag = etree.SubElement(parent_obj, 'ul')
    for item in list_obj:
        li_tag = etree.SubElement(ul_tag, 'li')
        gen_obj(item, li_tag)


def gen_dict(dict_obj, parent_obj):
    """
    Attaches representation of a given json dict object to parent_obj
    :param dict_obj: dict to convert
    :param parent_obj: etree.Element object to attach data to
    :return: None
    """
    for k, v in dict_obj.items():
        tag_name, tag_classes, tag_id = parse_tag_key(k)
        sub_element = etree.SubElement(parent_obj, tag_name)

        if tag_id is not None:
            sub_element.attrib['id'] = tag_id
        if tag_classes is not None:
            sub_element.attrib['class'] = ' '.join(tag_classes)

        gen_obj(v, sub_element)


def parse_tag_key(k):
    """
    Parses and splits tag key
    p.my-class1.my-class2#my-id
    :param k: tag key
    :return: tuple containing tag name, list of tag classes or None, tag id or None
    """

    # find and remove tag id
    elements = k.split('#', 1)
    if len(elements) == 1:
        tag_id = None
    else:
        tag_id = elements[1]

    # find and remove classes
    elements = elements[0].split('.', 1)
    if len(elements) == 1:
        tag_classes = None
    else:
        tag_classes = elements[1]

    tag_name = elements[0]
    if tag_classes is not None:
        tag_classes = tag_classes.split('.')

    return tag_name, tag_classes, tag_id
