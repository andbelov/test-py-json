import unittest
from task2 import gen_html


class TestTask2(unittest.TestCase):
    def test_html(self):
        self.assertEqual(
            gen_html(
                '''[
                {
                    "h3": "Title #1",
                    "div": "Hello, World 1!"
                }
                ]'''
            ),
            b'<h3>Title #1</h3><div>Hello, World 1!</div>'
        )

    def test_order(self):
        self.assertEqual(
            gen_html(
                '''[
                {
                    "div": "Hello, World 1!",
                    "h3": "Title #1"
                }
                ]'''
            ),
            b'<div>Hello, World 1!</div><h3>Title #1</h3>'
        )
