import unittest
from task1 import gen_html, WrongJsonTypeException

from main import run_app


class TestTask1(unittest.TestCase):
    def test_html(self):
        self.assertEqual(
            gen_html(
                '''[
                {
                    "title": "Title #1",
                    "body": "Hello, World 1!"
                },
                {
                    "title": "Title #2",
                    "body": "Hello, World 2!"
                }
                ]'''
            ),
            b'<h1>Title #1</h1><p>Hello, World 1!</p><h1>Title #2</h1><p>Hello, World 2!</p>'
        )

    def test_run_app(self):
        run_app(
            input_fn=lambda: '''
            [
                {
                    "title": "Title #1",
                    "body": "Hello, World 1!"
                }
            ]
            ''',
            output_fn=lambda x: self.assertEqual(x, b'<h1>Title #1</h1><p>Hello, World 1!</p>'),
            convert_fn=gen_html
        )

    def test_wrong_json_type(self):
        with self.assertRaises(WrongJsonTypeException):
            # not a list, but a dict
            gen_html('''
                {
                    "title": "Title #1",
                    "body": "Hello, World 1!"
                }
            ''')

    def test_missing_json_key(self):
        with self.assertRaises(KeyError):
            # missing mandatory keys
            gen_html('''
            [
                {
                    "h1": "Title #1",
                    "p": "Hello, World 1!"
                }
            ]
            ''')

    def test_unicode(self):
        self.assertEqual(
            gen_html(
                '''[
                {
                    "title": "Заголовок №1",
                    "body": "Здравствуй, Мир 1!"
                },
                {
                    "title": "Заголовок №2",
                    "body": "Здравствуй, Мир 2!"
                }
                ]'''
            ),
            '<h1>Заголовок №1</h1><p>Здравствуй, Мир 1!</p><h1>Заголовок №2</h1><p>Здравствуй, Мир 2!</p>'.encode()
        )
