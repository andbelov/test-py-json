import unittest
from task4 import gen_html


class TestTask4(unittest.TestCase):
    def test_html(self):
        self.assertEqual(
            gen_html(
                '''[
                    {
                        "span": "Title #1",
                        "content": [
                            {
                                "p": "Example 1",
                                "header": "header 1"
                            }
                        ]
                    },
                    {"div": "div 1"}
                ]'''
            ),
            b'<ul><li><span>Title #1</span><content><ul><li><p>Example 1</p><header>header 1</header></li></ul></content></li><li><div>div 1</div></li></ul>'
        )

    def test_html2(self):
        self.assertEqual(
            gen_html(
                '''{
                    "p":"hello1"
                }'''
            ),
            b'<p>hello1</p>'
        )
