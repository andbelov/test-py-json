import unittest
from task5 import gen_html


class TestTask5(unittest.TestCase):
    def test_html(self):
        self.assertEqual(
            gen_html(
                '''{
                    "p.my-class#my-id": "hello",
                    "p.my-class1.my-class2":"example<a>asd</a>"
                }'''
            ),
            b'<p id="my-id" class="my-class">hello</p><p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p>'
        )
