import unittest
from task3 import gen_html


class TestTask3(unittest.TestCase):
    def test_html(self):
        self.assertEqual(
            gen_html(
                '''[
                    {
                        "h3": "Title #1",
                        "div": "Hello, World 1!"
                    },
                    {
                        "h3": "Title #2",
                        "div": "Hello, World 2!"
                    }
                ]'''
            ),
            b'<ul><li><h3>Title #1</h3><div>Hello, World 1!</div></li><li><h3>Title #2</h3><div>Hello, World 2!</div></li></ul>'
        )
