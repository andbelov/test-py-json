import json
from collections import OrderedDict
from utils import create_tag


class WrongJsonTypeException(Exception):
    pass


def gen_html(data):
    # OrderedDict
    json_data = json.loads(data, object_pairs_hook=OrderedDict)
    result = bytes()

    if type(json_data) is not list:
        raise WrongJsonTypeException

    for d in json_data:
        for k, v in d.items():
            result += create_tag(k, v)

    return result
