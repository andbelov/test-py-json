import json
from collections import OrderedDict
from lxml import etree
from utils import create_tag


def gen_html(data):
    json_data = json.loads(data, object_pairs_hook=OrderedDict)

    # если не list, то стало быть dict?
    if type(json_data) is not list:
        for k, v in json_data.items():
            return create_tag(k, v)

    ul_tag = etree.Element('ul')
    for d in json_data:
        li_tag = etree.SubElement(ul_tag, 'li')
        for k, v in d.items():
            internal_tag = etree.SubElement(li_tag, k)
            internal_tag.text = v

    return etree.tostring(ul_tag, encoding='UTF-8', xml_declaration=False)
